﻿// © 2017 Naoki Nakagawa
#pragma once
#include <vector>
#include "GameLibrary.h"

enum Player {
	First,
	Second,
};

class Piece {
	PRIVATE static GameLibrary::Sprite pieceSprite;
	PRIVATE static GameLibrary::Sprite shadowSprite;
	PRIVATE static GameLibrary::Sprite rangeSprite;

	PUBLIC DirectX::XMFLOAT2 position;
	PUBLIC bool isPromote;
	PUBLIC Player owner;
	PRIVATE float scale;
	PRIVATE GameLibrary::Text name;

	PUBLIC Piece(const wchar_t* name, float scale);
	PUBLIC virtual ~Piece();
	PUBLIC virtual std::vector<bool> GatMovableCells(DirectX::XMINT2 cell) = 0;
	PROTECTED bool TrySetMovableCell(DirectX::XMINT2 cell, std::vector<bool>& movableCells, int x, int y);
	PROTECTED void SetAdvanceableCells(DirectX::XMINT2 cell, std::vector<bool>& movableCells, int x, int y);
	PROTECTED void SetGoldMovableCells(DirectX::XMINT2 cell, std::vector<bool>& movableCells);
	PUBLIC bool IsInRange(DirectX::XMINT2 position);
	PUBLIC virtual void Draw();
	PROTECTED void Draw(GameLibrary::Text& text, DirectX::XMFLOAT4 color);
};

class PromotablePiece : public Piece {
	PRIVATE GameLibrary::Text promotedName;

	PUBLIC PromotablePiece(const wchar_t* name, const wchar_t* promotedName, float scale);
	PUBLIC virtual void Draw();
};

class King : public Piece {
	PRIVATE GameLibrary::Text name2;

	PUBLIC King();
	PUBLIC std::vector<bool> GatMovableCells(DirectX::XMINT2 cell);
	PUBLIC void Draw();
};

class Rook : public PromotablePiece {
	PUBLIC Rook();
	PUBLIC std::vector<bool> GatMovableCells(DirectX::XMINT2 cell);
};

class Bishop : public PromotablePiece {
	PUBLIC Bishop();
	PUBLIC std::vector<bool> GatMovableCells(DirectX::XMINT2 cell);
};

class Gold : public Piece {
	PUBLIC Gold();
	PUBLIC std::vector<bool> GatMovableCells(DirectX::XMINT2 cell);
};

class Silver : public PromotablePiece {
	PUBLIC Silver();
	PUBLIC std::vector<bool> GatMovableCells(DirectX::XMINT2 cell);
};

class Knight : public PromotablePiece {
	PUBLIC Knight();
	PUBLIC std::vector<bool> GatMovableCells(DirectX::XMINT2 cell);
};

class Lance : public PromotablePiece {
	PUBLIC Lance();
	PUBLIC std::vector<bool> GatMovableCells(DirectX::XMINT2 cell);
};

class Pawn : public PromotablePiece {
	PUBLIC Pawn();
	PUBLIC std::vector<bool> GatMovableCells(DirectX::XMINT2 cell);
};

﻿// © 2017 Naoki Nakagawa
#include <typeinfo>
#include "Board.h"
#include "Piece.h"

using namespace std;
using namespace DirectX;
using namespace GameLibrary;

Sprite Piece::pieceSprite(L"assets/Piece.png");
Sprite Piece::shadowSprite(L"assets/Shadow.png");
Sprite Piece::rangeSprite(L"assets/Cell.png");

Piece::Piece(const wchar_t* name, float scale) :
	position(rand() % Game::GetSize().x, rand() % Game::GetSize().y),
	isPromote(false),
	owner(Player::First),
	scale(scale),
	name(name, L"衡山毛筆フォント行書") {
}

Piece::~Piece() {
}

bool Piece::TrySetMovableCell(XMINT2 cell, vector<bool>& movableCells, int x, int y) {
	if (owner == Player::Second) {
		y = -y;
	}

	if (!Board::GetInstance().IsInRange(cell.x + x, cell.y + y)) {
		return false;
	}

	Piece* to = Board::GetInstance().GetPiece(cell.x + x, cell.y + y);
	if (to != nullptr && owner == to->owner) {
		return false;
	}

	movableCells[Board::GetInstance().GetIndex(cell.x + x, cell.y + y)] = true;

	if (to != nullptr && owner != to->owner) {
		return false;
	}

	return true;
}

void Piece::SetAdvanceableCells(XMINT2 cell, vector<bool>& movableCells, int x, int y) {
	XMINT2 offset(x, y);
	while (TrySetMovableCell(cell, movableCells, offset.x, offset.y)) {
		offset.x += x;
		offset.y += y;
	}
}

void Piece::SetGoldMovableCells(XMINT2 cell, vector<bool>& movableCells) {
	TrySetMovableCell(cell, movableCells, -1, -1);
	TrySetMovableCell(cell, movableCells, 0, -1);
	TrySetMovableCell(cell, movableCells, 1, -1);
	TrySetMovableCell(cell, movableCells, -1, 0);
	TrySetMovableCell(cell, movableCells, 1, 0);
	TrySetMovableCell(cell, movableCells, 0, 1);
}

bool Piece::IsInRange(XMINT2 position) {
	XMFLOAT2 a(this->position.x - Board::GetInstance().GetCellSize() / 2.0f, this->position.y - Board::GetInstance().GetCellSize() / 2.0f);
	XMFLOAT2 b(this->position.x + Board::GetInstance().GetCellSize() / 2.0f, this->position.y + Board::GetInstance().GetCellSize() / 2.0f);

	if (position.x < a.x) return false;
	if (position.y < a.y) return false;
	if (position.x > b.x) return false;
	if (position.y > b.y) return false;

	return true;
}

void Piece::Draw() {
	Draw(name, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
}

void Piece::Draw(Text& text, XMFLOAT4 color) {
	switch (owner) {
	case Player::First:
		pieceSprite.angle = 0.0f;
		break;
	case Player::Second:
		pieceSprite.angle = 180.0f;
		break;
	}
	pieceSprite.position = position;
	pieceSprite.scale.x = pieceSprite.scale.y = Board::GetInstance().GetCellSize() / (float)pieceSprite.GetSize().x * scale;

	shadowSprite.position = XMFLOAT2(pieceSprite.position.x + 2.0f, pieceSprite.position.y + 5.0f);
	shadowSprite.angle = pieceSprite.angle;
	shadowSprite.scale.x = shadowSprite.scale.y = Board::GetInstance().GetCellSize() / (float)shadowSprite.GetSize().x * 1.2f * scale;
	shadowSprite.Draw();

	pieceSprite.Draw();

	text.position = pieceSprite.position;
	text.angle = pieceSprite.angle;
	text.scale.x = text.scale.y = Board::GetInstance().GetCellSize() / (float)text.GetSize().x * 0.8f * scale;
	text.color = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.5f);
	text.Draw();

	text.position.x += 1;
	text.position.y += 1;
	text.color = color;
	text.Draw();

	rangeSprite.position = pieceSprite.position;
	rangeSprite.scale.x = rangeSprite.scale.y = Board::GetInstance().GetCellSize() / (float)rangeSprite.GetSize().x;
	rangeSprite.color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
	//rangeSprite.Draw();
}

PromotablePiece::PromotablePiece(const wchar_t* name, const wchar_t* promotedName, float scale) :
	promotedName(promotedName, L"衡山毛筆フォント行書"),
	Piece(name, scale) {
}

void PromotablePiece::Draw() {
	if (isPromote) {
		Piece::Draw(promotedName, XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	}
	else {
		Piece::Draw();
	}
}

King::King() :
	name2(L"玉", L"衡山毛筆フォント行書"),
	Piece(L"王", 1.0f) {
}

vector<bool> King::GatMovableCells(XMINT2 cell) {
	vector<bool> movableCells = Board::GetFalseCells();
	TrySetMovableCell(cell, movableCells, -1, -1);
	TrySetMovableCell(cell, movableCells, 0, -1);
	TrySetMovableCell(cell, movableCells, 1, -1);
	TrySetMovableCell(cell, movableCells, -1, 0);
	TrySetMovableCell(cell, movableCells, 1, 0);
	TrySetMovableCell(cell, movableCells, -1, 1);
	TrySetMovableCell(cell, movableCells, 0, 1);
	TrySetMovableCell(cell, movableCells, 1, 1);
	return movableCells;
}

void King::Draw() {
	if (owner == Player::First) {
		Piece::Draw(name2, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	}
	else {
		Piece::Draw();
	}
}

Rook::Rook() :
	PromotablePiece(L"飛", L"竜", 0.95f) {
}

vector<bool> Rook::GatMovableCells(XMINT2 cell) {
	vector<bool> movableCells = Board::GetFalseCells();
	SetAdvanceableCells(cell, movableCells, 0, -1);
	SetAdvanceableCells(cell, movableCells, -1, 0);
	SetAdvanceableCells(cell, movableCells, 1, 0);
	SetAdvanceableCells(cell, movableCells, 0, 1);
	if (isPromote) {
		TrySetMovableCell(cell, movableCells, -1, -1);
		TrySetMovableCell(cell, movableCells, 1, -1);
		TrySetMovableCell(cell, movableCells, -1, 1);
		TrySetMovableCell(cell, movableCells, 1, 1);
	}
	return movableCells;
}

Bishop::Bishop() :
	PromotablePiece(L"角", L"馬", 0.95f) {
}

vector<bool> Bishop::GatMovableCells(XMINT2 cell) {
	vector<bool> movableCells = Board::GetFalseCells();
	SetAdvanceableCells(cell, movableCells, -1, -1);
	SetAdvanceableCells(cell, movableCells, 1, -1);
	SetAdvanceableCells(cell, movableCells, -1, 1);
	SetAdvanceableCells(cell, movableCells, 1, 1);
	if (isPromote) {
		TrySetMovableCell(cell, movableCells, 0, -1);
		TrySetMovableCell(cell, movableCells, -1, 0);
		TrySetMovableCell(cell, movableCells, 1, 0);
		TrySetMovableCell(cell, movableCells, 0, 1);
	}
	return movableCells;
}

Gold::Gold() :
	Piece(L"金", 0.9f) {
}

vector<bool> Gold::GatMovableCells(XMINT2 cell) {
	vector<bool> movableCells = Board::GetFalseCells();
	SetGoldMovableCells(cell, movableCells);
	return movableCells;
}

Silver::Silver() :
	PromotablePiece(L"銀", L"全", 0.85f) {
}

vector<bool> Silver::GatMovableCells(XMINT2 cell) {
	vector<bool> movableCells = Board::GetFalseCells();
	if (isPromote) {
		SetGoldMovableCells(cell, movableCells);
	}
	else {
		TrySetMovableCell(cell, movableCells, -1, -1);
		TrySetMovableCell(cell, movableCells, 0, -1);
		TrySetMovableCell(cell, movableCells, 1, -1);
		TrySetMovableCell(cell, movableCells, -1, 1);
		TrySetMovableCell(cell, movableCells, 1, 1);
	}
	return movableCells;
}

Knight::Knight() :
	PromotablePiece(L"桂", L"圭", 0.8f) {
}

vector<bool> Knight::GatMovableCells(XMINT2 cell) {
	vector<bool> movableCells = Board::GetFalseCells();
	if (isPromote) {
		SetGoldMovableCells(cell, movableCells);
	}
	else {
		TrySetMovableCell(cell, movableCells, -1, -2);
		TrySetMovableCell(cell, movableCells, 1, -2);
	}
	return movableCells;
}

Lance::Lance() :
	PromotablePiece(L"香", L"杏", 0.75f) {
}

vector<bool> Lance::GatMovableCells(XMINT2 cell) {
	vector<bool> movableCells = Board::GetFalseCells();
	if (isPromote) {
		SetGoldMovableCells(cell, movableCells);
	}
	else {
		SetAdvanceableCells(cell, movableCells, 0, -1);
	}
	return movableCells;
}

Pawn::Pawn() :
	PromotablePiece(L"歩", L"と", 0.7f) {
}

vector<bool> Pawn::GatMovableCells(XMINT2 cell) {
	vector<bool> movableCells = Board::GetFalseCells();
	if (isPromote) {
		SetGoldMovableCells(cell, movableCells);
	}
	else {
		TrySetMovableCell(cell, movableCells, 0, -1);
	}
	return movableCells;
}

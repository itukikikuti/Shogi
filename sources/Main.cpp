﻿// © 2017 Naoki Nakagawa
#include <functional>
#include <vector>
#include "GameLibrary.h"
#include "Piece.h"
#include "Board.h"

using namespace std;
using namespace DirectX;
using namespace GameLibrary;

class Scene {
	PUBLIC function<void()> Update;
	PRIVATE Player player;
	PRIVATE Piece* piece;

	PUBLIC Scene() :
		player(Player::First) {
		Set(&Scene::SelectPiece);
	}

	PUBLIC ~Scene() {
	}

	PRIVATE void Set(void (Scene::*&&func)()) {
		Update = std::bind(func, this);
	}
	
	PRIVATE void SelectPiece() {
		vector<bool> cells = Board::GetInstance().GetSelectableCells(player);

		if (Game::GetKeyDown(VK_LBUTTON)) {
			piece = Board::GetInstance().GetPieceByPosition(Game::GetMousePosition());

			if (piece != nullptr && piece->owner == player) {
				Set(&Scene::SelectMoveCell);
			}
		}

		Board::GetInstance().AlignPieceInterpolated();
		Board::GetInstance().Draw();
		Board::GetInstance().DrawLight(cells);
	}
	
	PRIVATE void SelectMoveCell() {
		//vector<bool> cells = piece->GatMovableCells(mouseCell);

		if (Game::GetKeyUp(VK_LBUTTON)) {
			Piece* to = Board::GetInstance().GetPieceByPosition(Game::GetMousePosition());

			if (to != nullptr && to->owner == player) {
				Set(&Scene::SelectPiece);
			}
			/*else if (!cells[Board::GetInstance().GetIndex(mouseCell.x, mouseCell.y)]) {
				Set(&Scene::SelectPiece);
			}*/
			else {
				Set(&Scene::MovePieceScene);
			}
		}

		Board::GetInstance().AlignPieceInterpolated();
		piece->position = XMFLOAT2(Game::GetMousePosition().x, Game::GetMousePosition().y);

		Board::GetInstance().Draw();
		//Board::GetInstance().DrawLight(cells);
	}
	
	PRIVATE void MovePieceScene() {
		//Board::GetInstance().MovePiece(piece, mouseCell.x, mouseCell.y);
		Set(&Scene::SelectPiece);
		if (player == Player::First) {
			player = Player::Second;
		}
		else {
			player = Player::First;
		}

		Board::GetInstance().AlignPieceInterpolated();
		Board::GetInstance().Draw();
	}
};

int Main() {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	Game::SetTitle(L"将棋");
	Game::AddFont(L"assets/衡山毛筆フォント行書.ttf");

	Scene scene;

	while (Game::Update()) {
		scene.Update();
	}

	return 0;
}

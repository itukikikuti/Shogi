﻿// © 2017 Naoki Nakagawa
#include <typeinfo>
#include <algorithm>
#include "Board.h"

using namespace std;
using namespace DirectX;
using namespace GameLibrary;

Sprite Board::boardSprite(L"assets/Board.png");
Sprite Board::reserveSprite(L"assets/Reserve.png");
Sprite Board::cellSprite(L"assets/Cell.png");
Sprite Board::lightSprite(L"assets/Light.png");

Board& Board::GetInstance() {
	static Board instance;
	return instance;
}

vector<bool> Board::GetFalseCells() {
	vector<bool> cells;
	for (int i = 0; i < Board::LENGTH; i++) {
		cells.push_back(false);
	}
	return cells;
}

Board::Board() {
	cellSprite.color = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	lightSprite.color = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.75f);

	SetPiece(0, 0, Player::Second, new Lance());
	SetPiece(1, 0, Player::Second, new Knight());
	SetPiece(2, 0, Player::Second, new Silver());
	SetPiece(3, 0, Player::Second, new Gold());
	SetPiece(4, 0, Player::Second, new King());
	SetPiece(5, 0, Player::Second, new Gold());
	SetPiece(6, 0, Player::Second, new Silver());
	SetPiece(7, 0, Player::Second, new Knight());
	SetPiece(8, 0, Player::Second, new Lance());

	SetPiece(1, 1, Player::Second, new Bishop());
	SetPiece(7, 1, Player::Second, new Rook());

	SetPiece(0, 2, Player::Second, new Pawn());
	SetPiece(1, 2, Player::Second, new Pawn());
	SetPiece(2, 2, Player::Second, new Pawn());
	SetPiece(3, 2, Player::Second, new Pawn());
	SetPiece(4, 2, Player::Second, new Pawn());
	SetPiece(5, 2, Player::Second, new Pawn());
	SetPiece(6, 2, Player::Second, new Pawn());
	SetPiece(7, 2, Player::Second, new Pawn());
	SetPiece(8, 2, Player::Second, new Pawn());
	

	SetPiece(0, 6, Player::First, new Pawn());
	SetPiece(1, 6, Player::First, new Pawn());
	SetPiece(2, 6, Player::First, new Pawn());
	SetPiece(3, 6, Player::First, new Pawn());
	SetPiece(4, 6, Player::First, new Pawn());
	SetPiece(5, 6, Player::First, new Pawn());
	SetPiece(6, 6, Player::First, new Pawn());
	SetPiece(7, 6, Player::First, new Pawn());
	SetPiece(8, 6, Player::First, new Pawn());

	SetPiece(1, 7, Player::First, new Rook());
	SetPiece(7, 7, Player::First, new Bishop());

	SetPiece(0, 8, Player::First, new Lance());
	SetPiece(1, 8, Player::First, new Knight());
	SetPiece(2, 8, Player::First, new Silver());
	SetPiece(3, 8, Player::First, new Gold());
	SetPiece(4, 8, Player::First, new King());
	SetPiece(5, 8, Player::First, new Gold());
	SetPiece(6, 8, Player::First, new Silver());
	SetPiece(7, 8, Player::First, new Knight());
	SetPiece(8, 8, Player::First, new Lance());
}

Board::~Board() {
	for (int i = 0; i < LENGTH; i++) {
		delete pieces[i];
	}
}

float Board::GetCellSize() {
	return (Game::GetSize().y - 50.0f) / SIZE;
}

XMFLOAT2 Board::CellToPosition(int x, int y) {
	XMFLOAT2 screenPosition(Game::GetSize().x / 2.0f, Game::GetSize().y / 2.0f);
	screenPosition.x += (SIZE / 2) * GetCellSize();
	screenPosition.y -= (SIZE / 2) * GetCellSize();
	screenPosition.x -= x * GetCellSize();
	screenPosition.y += y * GetCellSize();
	return screenPosition;
}

XMINT2 Board::PositionToCell(XMINT2 position) {
	XMFLOAT2 screenPosition(Game::GetSize().x / 2.0f - position.x, position.y - Game::GetSize().y / 2.0f);
	screenPosition.x += (SIZE / 2) * GetCellSize();
	screenPosition.y += (SIZE / 2) * GetCellSize();
	screenPosition.x /= GetCellSize();
	screenPosition.y /= GetCellSize();
	position.x = clamp((int)round(screenPosition.x), 0, SIZE - 1);
	position.y = clamp((int)round(screenPosition.y), 0, SIZE - 1);
	return position;
}

int Board::GetIndex(int x, int y) {
	return x + y * SIZE;
}

int Board::FindIndex(Piece* piece) {
	for (int i = 0; i < Board::LENGTH; i++) {
		if (GetPiece(i) == piece) {
			return i;
		}
	}
	throw "Not faund.";
}

bool Board::IsInRange(int x, int y) {
	return 0 <= x && x < SIZE && 0 <= y && y < SIZE;
}

Piece* Board::GetPiece(int i) {
	return pieces[i];
}

Piece* Board::GetPiece(int x, int y) {
	return pieces[GetIndex(x, y)];
}

Piece* Board::GetPieceByPosition(XMINT2 position) {
	float nearDistance = 99999.0f;
	int nearIndex = -1;

	for (int i = 0; i < Board::LENGTH; i++) {
		if (GetPiece(i) == nullptr) {
			continue;
		}

		if (!GetPiece(i)->IsInRange(position)) {
			continue;
		}

		float distance = pow(position.x - GetPiece(i)->position.x, 2) * pow(position.y - GetPiece(i)->position.y, 2);

		if (distance < nearDistance) {
			nearDistance = distance;
			nearIndex = i;
		}
	}

	if (nearIndex > -1) {
		return GetPiece(nearIndex);
	}

	return nullptr;
}

void Board::SetPiece(int x, int y, Player owner, Piece* piece) {
	pieces[GetIndex(x, y)] = piece;
	piece->owner = owner;
}

void Board::MovePiece(Piece* piece, int x, int y) {
	Piece* to = GetPiece(x, y);

	if (to != nullptr && to->owner != piece->owner) {
		delete to;
		to = nullptr;
	}

	pieces[FindIndex(piece)] = to;
	pieces[GetIndex(x, y)] = piece;

	if (piece->owner == Player::First && y <= 2) {
		PromotePiece(piece, true);
	}
	if (piece->owner == Player::Second && y >= 6) {
		PromotePiece(piece, true);
	}
}

bool Board::PromotePiece(Piece* piece, bool isPromote) {
	piece->isPromote = isPromote;
	return true;
}

vector<bool> Board::GetSelectableCells(Player player) {
	vector<bool> cells = GetFalseCells();
	for (int x = 0; x < SIZE; x++) {
		for (int y = 0; y < SIZE; y++) {
			if (GetPiece(x, y) == nullptr) {
				continue;
			}

			if (GetPiece(x, y)->owner != player) {
				continue;
			}

			cells[GetIndex(x, y)] = true;
		}
	}
	return cells;
}

void Board::AlignPieceInterpolated() {
	for (int x = 0; x < SIZE; x++) {
		for (int y = 0; y < SIZE; y++) {
			if (GetPiece(x, y) == nullptr) {
				continue;
			}

			XMFLOAT2 a = CellToPosition(x, y), b = GetPiece(x, y)->position;
			float t = Game::GetDeltaTime() / 0.1f;
			GetPiece(x, y)->position = XMFLOAT2(b.x + (a.x - b.x) * t, b.y + (a.y - b.y) * t);
		}
	}
}

void Board::Draw() {
	boardSprite.position = XMFLOAT2(Game::GetSize().x / 2.0f, Game::GetSize().y / 2.0f);
	boardSprite.scale.x = boardSprite.scale.y = Game::GetSize().y / (float)boardSprite.GetSize().y;
	boardSprite.Draw();

	float reserveSize = (Game::GetSize().x - Game::GetSize().y) / 2.0f;
	reserveSprite.scale.x = reserveSprite.scale.y = reserveSize / reserveSprite.GetSize().x;

	reserveSprite.position = XMFLOAT2(Game::GetSize().x - reserveSize / 2.0f, Game::GetSize().y - reserveSize / 2.0f);
	reserveSprite.Draw();

	reserveSprite.position = XMFLOAT2(reserveSize / 2.0f, reserveSize / 2.0f);
	reserveSprite.Draw();

	for (int x = 0; x < SIZE; x++) {
		for (int y = 0; y < SIZE; y++) {
			cellSprite.position = CellToPosition(x, y);
			cellSprite.scale.x = cellSprite.scale.y = GetCellSize() / (cellSprite.GetSize().x - 1);
			cellSprite.Draw();
		}
	}

	for (int x = 0; x < SIZE; x++) {
		for (int y = 0; y < SIZE; y++) {
			if (GetPiece(x, y) == nullptr) {
				continue;
			}
			GetPiece(x, y)->Draw();
		}
	}
}

void Board::DrawLight(std::vector<bool> cells) {
	lightSprite.scale.x = lightSprite.scale.y = GetCellSize() / lightSprite.GetSize().x;

	for (int x = 0; x < SIZE; x++) {
		for (int y = 0; y < SIZE; y++) {
			if (cells[GetIndex(x, y)]) {
				lightSprite.position = CellToPosition(x, y);
				lightSprite.position.y -= GetCellSize() / 2.0f;
				lightSprite.Draw();
			}
		}
	}
}

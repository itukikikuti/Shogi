﻿// © 2017 Naoki Nakagawa
#pragma once
#include <vector>
#include "GameLibrary.h"
#include "Piece.h"

class Board {
	PUBLIC static const int SIZE = 9;
	PUBLIC static constexpr size_t LENGTH = SIZE * SIZE;
	PRIVATE static GameLibrary::Sprite boardSprite;
	PRIVATE static GameLibrary::Sprite reserveSprite;
	PRIVATE static GameLibrary::Sprite cellSprite;
	PRIVATE static GameLibrary::Sprite lightSprite;
	PUBLIC static Board& GetInstance();
	PUBLIC static std::vector<bool> GetFalseCells();

	PRIVATE Piece* pieces[SIZE * SIZE];

	PRIVATE Board();
	PRIVATE ~Board();
	PUBLIC float GetCellSize();
	PUBLIC DirectX::XMFLOAT2 CellToPosition(int x, int y);
	PUBLIC DirectX::XMINT2 PositionToCell(DirectX::XMINT2 position);
	PUBLIC int GetIndex(int x, int y);
	PUBLIC int FindIndex(Piece* piece);
	PUBLIC bool IsInRange(int x, int y);
	PUBLIC Piece* GetPiece(int i);
	PUBLIC Piece* GetPiece(int x, int y);
	PUBLIC Piece* GetPieceByPosition(DirectX::XMINT2 position);
	PUBLIC void SetPiece(int x, int y, Player owner, Piece* piece);
	PUBLIC void MovePiece(Piece* piece, int x, int y);
	PUBLIC bool PromotePiece(Piece* piece, bool isPromote);
	PUBLIC std::vector<bool> GetSelectableCells(Player player);
	PUBLIC void AlignPieceInterpolated();
	PUBLIC void Draw();
	PUBLIC void DrawLight(std::vector<bool> cells);
};
